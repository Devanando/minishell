# minishell

minishell is a project in the Codam. The task is to implement a very small
subset of a shell in C.

## Compilation

Make sure you have the `meson` package installed. You can optionally install
`criterion` for unit testing.

```sh
meson build
cd build
ninja
./minishell
```

When installing `criterion` in a non-standard directory, make sure to point
`$PKG_CONFIG_PATH` to the relevant pkgconfig directories. On most school iMacs
this would be `$HOME/.brew/lib/pkgconfig:$HOME/.brew/share/pkgconfig`. You can
run `meson test -v` to run all unit tests.
