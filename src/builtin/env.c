/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#include "env.h"
#include "builtin.h"

int		builtin_env(int argc, char **argv, struct s_env **env)
{
	if (*env == NULL)
		return (0);
	ft_printf("%s=%s%c", (*env)->key, (*env)->value, '\n');
	return (builtin_env(argc, argv, &(*env)->next));
}

int		builtin_setenv(int argc, char **argv, t_env **env)
{
	int		i;
	char	**key_value;

	i = 1;
	while (i < argc)
	{
		if (ft_strchr(argv[i], '='))
		{
			key_value = ft_strsplit(argv[i], '=');
			if (!env_is_valid_key(key_value[0]))
				ft_dprintf(2, "setenv: invalid variable name '%s'\n", argv[i]);
			else
			{
				if (key_value[1] == NULL)
					env_set(env, key_value[0], "");
				else
					env_set(env, key_value[0], key_value[1]);
				ft_arraydel((void ***)&key_value, ft_memdel);
			}
		}
		else
			ft_dprintf(2, "setenv: invalid variable name: '%s'\n", argv[i]);
		i++;
	}
	return (0);
}

int		builtin_unsetenv(int argc, char **argv, t_env **env)
{
	int		i;

	i = 1;
	while (i < argc)
	{
		if (!env_is_valid_key(argv[i]))
			ft_dprintf(2, "unsetenv: invalid variable name: '%s'\n", argv[i]);
		else
			env_unset(env, argv[i]);
		i++;
	}
	return (0);
}
