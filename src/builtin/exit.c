/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#ifdef __linux__
# include <linux/limits.h>
#else
# include <limits.h>
#endif
#include <unistd.h>
#include <stdlib.h>
#include "env.h"
#include "builtin.h"

int		builtin_exit(int argc, char **argv, t_env **env)
{
	char *prev_status;

	prev_status = env_get_unsafe_ptr(*env, "STATUS");
	if (argc == 1)
		exit(prev_status ? ft_atoi(prev_status) : 0);
	else
		exit(ft_atoi(argv[1]));
}
