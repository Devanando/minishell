/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <libft.h>
#ifdef __linux__
# include <linux/limits.h>
#else
# include <limits.h>
#endif
#include <unistd.h>
#include "env.h"
#include "builtin.h"

static int	cd(t_env **env, char *old_path, char *new_path)
{
	if (chdir(new_path) == -1)
		return (-1);
	env_set(env, "OLDPWD", old_path);
	env_set(env, "PWD", new_path);
	return (0);
}

int			builtin_cd(int argc, char **argv, t_env **env)
{
	char	old_path[PATH_MAX];
	char	new_path[PATH_MAX];

	getcwd(old_path, PATH_MAX);
	if (argc > 2)
		return (ft_eprintf(1, "cd: to many arguments\n"));
	if (argc == 1)
	{
		if (!env_has(*env, "HOME"))
			return (ft_eprintf(1, "cd: $HOME not set\n"));
		ft_strlcpy(new_path, env_get_unsafe_ptr(*env, "HOME"), PATH_MAX);
	}
	else if (ft_strcmp(argv[1], "-") == 0)
	{
		if (!env_has(*env, "OLDPWD"))
			return (ft_eprintf(1, "cd: $OLDPWD not set\n"));
		ft_strlcpy(new_path, env_get_unsafe_ptr(*env, "OLDPWD"), PATH_MAX);
		ft_printf("%s\n", new_path);
	}
	else
		ft_strlcpy(new_path, argv[1], PATH_MAX);
	if (cd(env, old_path, new_path) == -1)
		return (ft_eprintf(1, "cd: unable to change dir '%s'\n", new_path));
	return (0);
}
