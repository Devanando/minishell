/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>
#include <ft_printf.h>
#include <sys/stat.h>
#include "env.h"
#include "path.h"

static struct s_program	get_executable(char *pathname)
{
	struct stat			statbuf;
	struct s_program	program;

	ft_memset(&program, '\0', sizeof(struct s_program));
	if (stat(pathname, &statbuf) == -1)
		program.type = NOTFOUND;
	else if (S_ISREG(statbuf.st_mode) && access(pathname, X_OK) == 0)
	{
		program.type = EXECUTABLE;
		ft_strncpy(program.pathname, pathname, PATH_MAX);
	}
	else
		program.type = NOTFOUND;
	return (program);
}

struct s_builtin_map	g_builtin_map[] = {
	{"cd", builtin_cd},
	{"env", builtin_env},
	{"setenv", builtin_setenv},
	{"unsetenv", builtin_unsetenv},
	{"echo", builtin_echo},
	{"exit", builtin_exit},
};

static struct s_program	get_builtin(char *name)
{
	size_t i;

	i = 0;
	while (i < sizeof(g_builtin_map) / sizeof(g_builtin_map[0]))
	{
		if (ft_strcmp(name, g_builtin_map[i].name) == 0)
		{
			return ((struct s_program){
				.type = BUILTIN,
				.builtin_func = g_builtin_map[i].func,
			});
		}
		i++;
	}
	return (S_PROGRAM_NOTFOUND);
}

static struct s_program	find_program_(char *env_path, char *name)
{
	char				*pathsplit_ptr;
	char				pathname[PATH_MAX];
	struct s_program	program;

	if (env_path == NULL)
		return (S_PROGRAM_NOTFOUND);
	pathsplit_ptr = ft_strchr(env_path, ':');
	if (pathsplit_ptr != NULL)
		*pathsplit_ptr = '\0';
	ft_snprintf(pathname, PATH_MAX, "%s/%s", env_path, name);
	program = get_executable(pathname);
	if (program.type != NOTFOUND)
		return (program);
	if (pathsplit_ptr == NULL)
		return (S_PROGRAM_NOTFOUND);
	return (find_program_(pathsplit_ptr + 1, name));
}

/*
** Find the program to execute.
**
** If argv0 contains a `/` it will use argv0 as the path of the executable,
** checking that it exists and we have the right to execute it. The type will
** be EXECUTABLE.
**
** If argv0 is equal to a builtin, it will return a function pointer to said
** builtin. The type will be BUILTIN.
**
** If the PATH is not NULL, it will search for the executable in the path. The
** type will be EXECUTABLE.
**
** If all fails, the type will be NOTFOUND.
*/

struct s_program		find_program(char *env_path, char *argv0)
{
	struct s_program program;

	if (ft_strchr(argv0, '/'))
		return (get_executable(argv0));
	program = get_builtin(argv0);
	if (program.type != NOTFOUND)
		return (program);
	return (find_program_(env_path, argv0));
}
