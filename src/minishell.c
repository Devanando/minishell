/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_printf.h>
#include "minishell.h"
#include "execute.h"

static int			count_argv(char **argv)
{
	int argc;

	argc = 0;
	while (argv[argc] != NULL)
		argc++;
	return (argc);
}

static char			*prompt(void)
{
	char	*line;
	int		ret;

	ft_printf("%{cyan}cetush%{reset} 🐬 > ");
	ret = ft_getline(0, &line);
	if (ret == 0)
	{
		ft_printf("exit\n");
		return (ft_strdup("exit"));
	}
	if (ret == -1)
		return (NULL);
	return (line);
}

static t_exit_code	exec_line(t_env **env, char *line)
{
	int			argc;
	char		**argv;
	t_exit_code	exit_code;

	argv = ft_strfoversplit(line, ft_isspace);
	if (argv == NULL)
		return (ft_eprintf(255, "cetush: ft_strfoversplit: out of memory\n"));
	expand(*env, &argv);
	if (argv == NULL)
		return (ft_eprintf(255, "cetush: expand: out of memory\n"));
	argc = count_argv(argv);
	exit_code = execute(argc, argv, env);
	ft_arraydel((void ***)&argv, ft_memdel);
	return (exit_code);
}

void				cetush(t_env **env)
{
	char *line;
	char *status;

	line = prompt();
	if (line == NULL)
	{
		ft_dprintf(2, "cetush: unable to read input\n");
		env_set(env, "STATUS", "255");
		return ;
	}
	if (ft_strdropwhile(line, ft_isspace)[0] == '\0')
	{
		ft_strdel(&line);
		return ;
	}
	ft_asprintf(&status, "%hhu", exec_line(env, line));
	if (status != NULL)
		env_set(env, "STATUS", status);
	else
		ft_dprintf(2, "cetush: ft_asprintf: out of memory\n");
	ft_strdel(&status);
	ft_strdel(&line);
}
