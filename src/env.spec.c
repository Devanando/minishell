/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <ft_printf.h>
#include "env.h"

Test(env_new, returns_null_when_no_env)
{
	char *nothing = NULL;
	cr_assert_eq(env_new(&nothing), NULL);
}

Test(env_new, works_with_2_values)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	cr_assert_str_eq(env->key, "foo");
	cr_assert_str_eq(env->value, "bar");
	cr_assert_str_eq(env->next->key, "FOO");
	cr_assert_str_eq(env->next->value, "BAR");
	cr_assert_eq(env->next->next, NULL);

	env_free(&env);
}

Test(env_get, returns_null_when_not_set)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	cr_assert_eq(env_get(env, "BAR"), NULL);

	env_free(&env);
}

Test(env_get, returns_value_when_set)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	char *res = env_get(env, "FOO");
	cr_assert_str_eq(res, "BAR");

	ft_strdel(&res);
	env_free(&env);
}

Test(env_set, inserts_value_when_not_exists)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	cr_expect_eq(env_set(&env, "BAR", "FOO"), 0);
	char *res = env_get(env, "BAR");
	cr_expect_str_eq(res, "FOO");

	ft_strdel(&res);
	env_free(&env);
}

Test(env_set, modifies_value_when_exists)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	cr_expect_eq(env_set(&env, "FOO", "BAR2"), 0);
	char *res = env_get(env, "FOO");
	cr_expect_str_eq(res, "BAR2");

	ft_strdel(&res);
	env_free(&env);
}

Test(env_unset, returns_error_when_not_exists)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	cr_expect_eq(env_unset(&env, "BAR"), -1);

	env_free(&env);
}

Test(env_unset, removes_pair_when_exists)
{
	char *main_env[] = {
		"FOO=BAR",
		"foo=bar",
		NULL
	};

	t_env *env = env_new(main_env);

	cr_expect_eq(env_unset(&env, "FOO"), 0);
	cr_expect_eq(env_get(env, "FOO"), NULL);

	env_free(&env);
}
