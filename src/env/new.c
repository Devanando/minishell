/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "env.h"

static t_env	*env_new_tail(char **envp, struct s_env *tail)
{
	char			**splitted;
	struct s_env	*head;

	if (*envp == NULL)
		return (tail);
	head = ft_memalloc(sizeof(*head));
	if (head == NULL)
		return (NULL);
	splitted = ft_strsplit(*envp, '=');
	if (splitted == NULL)
	{
		ft_memdel((void **)&head);
		return (NULL);
	}
	head->key = splitted[0];
	head->value = splitted[1];
	head->next = tail;
	ft_memdel((void **)&splitted);
	return (env_new_tail(envp + 1, head));
}

t_env			*env_new(char **envp)
{
	return (env_new_tail(envp, NULL));
}

void			env_free(struct s_env **head)
{
	if ((*head)->next != NULL)
		env_free(&(*head)->next);
	ft_strdel(&(*head)->key);
	ft_strdel(&(*head)->value);
	ft_memdel((void **)head);
}
