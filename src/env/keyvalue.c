/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include "env.h"

char		*env_get_unsafe_ptr(struct s_env *env, char *key)
{
	if (env == NULL)
		return (NULL);
	if (ft_strcmp(env->key, key) == 0)
		return (env->value);
	return (env_get_unsafe_ptr(env->next, key));
}

char		*env_get(struct s_env *env, char *key)
{
	char	*env_ptr;

	env_ptr = env_get_unsafe_ptr(env, key);
	if (env_ptr == NULL)
		return (NULL);
	return (ft_strdup(env_ptr));
}

int			env_set(struct s_env **env, char *key, char *value)
{
	struct s_env *head;

	(void)env_unset(env, key);
	head = ft_memalloc(sizeof(*head));
	if (head == NULL)
		return (-1);
	head->key = ft_strdup(key);
	head->value = ft_strdup(value);
	head->next = *env;
	if (head->key == NULL || head->value == NULL)
	{
		ft_strdel(&head->key);
		ft_strdel(&head->value);
		ft_memdel((void **)&head);
		return (-1);
	}
	*env = head;
	return (0);
}

int			env_unset(struct s_env **head, char *key)
{
	struct s_env *next;

	if (*head == NULL)
		return (-1);
	if (ft_strcmp((*head)->key, key) != 0)
		return (env_unset(&(*head)->next, key));
	next = (*head)->next;
	ft_strdel(&(*head)->key);
	ft_strdel(&(*head)->value);
	ft_memdel((void **)head);
	*head = next;
	return (0);
}

int			env_has(struct s_env *env, char *key)
{
	return (env_get_unsafe_ptr(env, key) != NULL);
}
