/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_printf.h>
#include "env.h"

static int	env_length_tail(struct s_env *env, int state)
{
	if (env == NULL)
		return (state);
	return (env_length_tail(env->next, state + 1));
}

int			env_length(struct s_env *env)
{
	return (env_length_tail(env, 0));
}

char		**env_to_envp(struct s_env *env)
{
	char	**envp;
	int		i;

	envp = ft_memalloc((env_length(env) + 1) * sizeof(*envp));
	if (envp == NULL)
		return (NULL);
	i = 0;
	while (env != NULL)
	{
		if (ft_asprintf(&envp[i], "%s=%s", env->key, env->value) == -1)
		{
			ft_arraydel((void ***)&envp, ft_memdel);
			return (NULL);
		}
		i++;
		env = env->next;
	}
	return (envp);
}

int			env_is_valid_key(char *key)
{
	size_t	key_len;
	size_t	i;
	int		is_valid;

	key_len = ft_strlen(key);
	i = 0;
	while (i < key_len)
	{
		is_valid = (ft_isalnum(key[i]) || key[i] == '_');
		if (i == 0 && ft_isdigit(key[i]))
			is_valid = 0;
		if (!is_valid)
			return (0);
		i++;
	}
	return (1);
}
