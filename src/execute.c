/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <ft_printf.h>
#include <unistd.h>
#include <sys/wait.h>
#include "minishell.h"
#include "path.h"
#include "env.h"

static struct s_program	resolve(char *argv0, t_env *env)
{
	char				*path;
	struct s_program	program;

	path = env_get(env, "PATH");
	program = find_program(path, argv0);
	ft_strdel(&path);
	return (program);
}

static t_exit_code		fork_and_run(char *pathname, char **argv, char **envp)
{
	pid_t	pid;
	int		stat_loc;

	pid = fork();
	if (pid == 0)
	{
		execve(pathname, argv, envp);
		return (ft_eprintf(255, "cetush: unable to execute '%s'\n", pathname));
	}
	else
	{
		waitpid(pid, &stat_loc, 0);
		return (WEXITSTATUS(stat_loc));
	}
}

t_exit_code				execute(int argc, char **argv, t_env **env)
{
	char				**envp;
	struct s_program	program;
	t_exit_code			exit_code;

	program = resolve(argv[0], *env);
	if (program.type == BUILTIN)
	{
		return (program.builtin_func(argc, argv, env));
	}
	else if (program.type == EXECUTABLE)
	{
		envp = env_to_envp(*env);
		if (envp == NULL)
			return (ft_eprintf(255, "cetush: env_to_envp: out of memory\n"));
		exit_code = fork_and_run(program.pathname, argv, envp);
		ft_arraydel((void ***)&envp, ft_memdel);
		return (exit_code);
	}
	else
	{
		return (ft_eprintf(255,
			"cetush: unable to find executable or builtin '%s'\n", argv[0]));
	}
}
