/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_H
# define BUILTIN_H

typedef int		t_builtin_func(int argc, char **argv, t_env **env);

struct			s_builtin_map
{
	char			*name;
	t_builtin_func	*func;
};

int				builtin_cd(int argc, char **argv, t_env **env);
int				builtin_echo(int argc, char **argv, t_env **env);
int				builtin_env(int argc, char **argv, t_env **env);
int				builtin_setenv(int argc, char **argv, t_env **env);
int				builtin_unsetenv(int argc, char **argv, t_env **env);
int				builtin_exit(int argc, char **argv, t_env **env);

#endif
