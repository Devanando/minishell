/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell                                          :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*       nloomans <nloomans@student.codam.nl>        +#+                      */
/*                                                  #+#    #+#                */
/*   License: MIT                                  ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATH_H
# define PATH_H

# ifdef __linux__
#  include <linux/limits.h>
# else
#  include <limits.h>
# endif
# include "env.h"
# include "builtin.h"

# define S_PROGRAM_NOTFOUND (struct s_program){ .type = NOTFOUND }

enum				e_program_type
{
	NOTFOUND,
	BUILTIN,
	EXECUTABLE,
};

struct				s_program
{
	enum e_program_type	type;
	t_builtin_func		*builtin_func;
	char				pathname[PATH_MAX];
};

struct s_program	find_program(char *env_path, char *name);

#endif
