/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minishell.h                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: devanando <devanando@student.codam.nl>       +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/10 19:16:42 by devanando      #+#    #+#                */
/*   Updated: 2019/10/10 19:16:42 by devanando     ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "env.h"

typedef unsigned char	t_exit_code;
void					cetush(t_env **env);
void					expand(t_env *env, char ***argv);

#endif
